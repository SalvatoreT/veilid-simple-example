use std::sync::Arc;
use veilid_core::VeilidUpdate::{AppMessage, Network};
use veilid_core::{VeilidConfigBlockStore, VeilidConfigInner, VeilidConfigProtectedStore, VeilidConfigTableStore, VeilidUpdate};

#[tokio::main]
async fn main() {
    let update_callback = Arc::new(move |update: VeilidUpdate| {
        match update {
            AppMessage(msg) => {
                println!("Message: {}", String::from_utf8_lossy(msg.message().into()));
            }
            Network(msg) => {
                println!("Network: Peers {:}, bytes/sec [{} up] [{} down]", msg.peers.iter().count(), msg.bps_up, msg.bps_down)
            }
            _ => {
                println!("{:?}", update)
            }
        };
    });

    let config = VeilidConfigInner {
        program_name: "Example Veilid".into(),
        namespace: "veilid-simple-example".into(),
        protected_store: VeilidConfigProtectedStore {
            always_use_insecure_storage: true,
            directory: "./.veilid/block_store".into(),
            ..Default::default()
        },
        block_store: VeilidConfigBlockStore {
            directory: "./.veilid/block_store".into(),
            ..Default::default()
        },
        table_store: VeilidConfigTableStore {
            directory: "./.veilid/table_store".into(),
            ..Default::default()
        },
        ..Default::default()
    };

    let veilid = veilid_core::api_startup_config(update_callback, config).await.unwrap();
    println!("Node ID: {}", veilid.config().unwrap().get_veilid_state().config.network.routing_table.node_id);
    veilid.attach().await.unwrap();
    // While CTRL+C is not pressed, keep running
    tokio::signal::ctrl_c().await.unwrap();
    veilid.shutdown().await;
}
