# Veilid Simple Example

Basic example of a Veilid server.

It's currently the `main` branch of `veilid-core` until the release `> 2.5.0` comes out.

# Run

```shell
cargo run --
# CTRL + C to stop
```